import argparse
import statistics
import sounddevice as sd
import soundfile as sf
from scipy.io.wavfile import write
from scipy.io import loadmat
from scipy.fftpack import fft
import time

# --------------------------------------------------------

description='''This script performs letter recognition procedure by comparing recording parameters to parameters of 
pre-recorded signals. These signals represent letters A, U and I.
'''

def parserFunction():
    parser = argparse.ArgumentParser(description = description)
    parser.add_argument('mode',     type=str,    help=u'''Recognition mode: S - Standard detection mode; K - KNN detection mode''')

    args = parser.parse_args()
    return args

def parameterCalc(data, threshold):
    mean = []
    median = []
    std = []
    
    for i in range(len(data)):
        idx = []
        fft_tmp = abs(fft(data[i]))
        
        for j in range(int(len(fft_tmp)/2)):
            if fft_tmp[j] > threshold:
                idx.append(j)
                
        mean.append(statistics.mean(idx))  
        median.append(statistics.median(idx))
        std.append(statistics.stdev(idx))
    
    return mean, median, std

def recordVoice(filename):
    fs = 44100  # Sample rate
    seconds = 3  # Duration of recording

    print("Recording will begin in 3 seconds . . .")
    
    time.sleep(seconds)
    
    print("Recording. . .")
    myrecording = sd.rec(int(seconds * fs), samplerate=fs, channels=2)
    sd.wait()  # Wait until recording is finished
    
    print("Recording finished !")
    write(filename, fs, myrecording)  # Save as WAV file 

def readSignal(filename):
    # Extract data and sampling rate from file
    data, fs = sf.read(filename, dtype='float32')  
    
    tmp = data.tolist()
    
    signal = [row[0] for row in tmp]
    
    return signal

def signalCalc(data, threshold):
    idx = []
    
    fft_tmp = abs(fft(data))
    
    for j in range(int(len(fft_tmp)/2)):
        if fft_tmp[j] > threshold:
            idx.append(j)
            
    mean = statistics.mean(idx)
    median = statistics.median(idx)
    std = statistics.stdev(idx)
    
    return mean, median, std   

def matchFunction(signal_batch, signal, threshold):
    meanB, medianB, stdB = parameterCalc(signal_batch, threshold)
    
    AvgMeanB = statistics.mean(meanB)
    AvgMedianB = statistics.mean(medianB)
    AvgStdB = statistics.mean(stdB)
    
    meanSig, medianSig, stdSig = signalCalc(signal, threshold)
    
    distMean = abs(AvgMeanB - meanSig)
    distMedian = abs(AvgMedianB - medianSig)
    distStd = abs(AvgStdB - stdSig)
    
    dist = pow((pow(distMean, 2) + pow(distMedian, 2) + pow(distStd, 2)), 0.5)
    
    return dist

def assignSignal(dist_A, dist_U, dist_I):
    if dist_A < dist_U and dist_A < dist_I:
        print("This is letter A")
    elif dist_U < dist_I:
        print("This is letter U")
    else:
        print("This is letter I")

def sppechRecognition(signalA, signalU, signalI, thr):
    # Recording speech
    recordVoice('temp_file.wav')
    # Importing file
    recording = readSignal('temp_file.wav')
    
    for i in range(len(recording)):
        if i < 3000:
            recording[i] = 0
    
    if max(recording) < 0.1:
        print("No signal was recorded!")
    else:   
        distA = matchFunction(signalA, recording, thr)
        distU = matchFunction(signalU, recording, thr)
        distI = matchFunction(signalI, recording, thr)

        assignSignal(distA, distU, distI)


def paramVector(signal, signal_batch, threshold):
    # Calculating signal parameters
    meanSig, medianSig, stdSig = signalCalc(signal, threshold)
    
    # Estimating letter parameters
    meanB, medianB, stdB = parameterCalc(signal_batch, threshold)
    
    # Distance calculation
    
    dist = []
    
    for i in range(len(meanB)):
        D1 = meanSig - meanB[i]
        D2 = medianSig - medianB[i]
        D3 = stdSig - stdB[i]
        
        dist.append(pow((pow(D1, 2) + pow(D2, 2) + pow(D3, 2)), 0.5))
        
    return dist

def knnClassification(signal, signalA, signalU, signalI, threshold):
    distA = paramVector(signal, signalA, threshold)
    distU = paramVector(signal, signalU, threshold)
    distI = paramVector(signal, signalI, threshold)
    
    assignSignal(min(distA), min(distU), min(distI))

def speechRecognitionKNN(signalA, signalU, signalI, threshold):
    # Recording speech
    recordVoice('temp_file.wav')
    # Importing file
    recording = readSignal('temp_file.wav')
    
    for i in range(len(recording)):
        if i < 3000:
            recording[i] = 0

    if max(recording) < 0.1:
        print("No signal was recorded!")
    else:       
        knnClassification(recording, signalA, signalU, signalI, threshold)
    

def main(args):
    # Importing Signals
    sig = loadmat('./Recordings/A_recording.mat')
    dataA = sig['xA']
    signalA = dataA.tolist()

    sig = loadmat('./Recordings/U_recording.mat')
    dataU = sig['xU']
    signalU = dataU.tolist()    

    sig = loadmat('./Recordings/I_recording.mat')
    dataI = sig['xI']
    signalI = dataI.tolist()

    det_mode = vars(args)['mode']

    if det_mode == "S":
        print("Running letter recognition in standard mode")
        sppechRecognition(signalA, signalU, signalI, 200)
    elif det_mode == "K":
        print("Running letter recognition in KNN mode")
        speechRecognitionKNN(signalA, signalU, signalI, 200)
    else:
        print("Wrong argument type! Use only predefined options !")

if __name__ == '__main__':
    
    args = parserFunction()
    main(args)