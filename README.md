<div align="center">

# Speech Recognition - Letter Detection

</div>

<div align="center">

<br><b>Author:</b> Arkadiusz Marta</br>
<br><b>Country:</b> Poland</br>
<br><b>Faculty:</b> Mechanical Engineering and Robotics Department</br>
<br><b>Field of study:</b> Mechatronics Engineering</br>

</div>

---

## Summary

<div align="justify">

This project allows for performing simple letter recognition. It distinguishes only between letters <b>A</b>, <b>U</b> and <b>I</b>.
The user records their own voice, which is then analyzed and compared with pre-recorded voice samples and their parameters. 
After the analysis is finished the user receives an information about the results. 

</div>

---
### Required libraries

- <b>argparse</b>
    - Used to generate help and usage message 
- <b>statistics</b>
    - This module is used to calculate statictical parameters of signals - average, standard deviation etc. 
- <b>sounddevice</b>
    -  This module is used for recording user's voice
    - Installation:

```shell
conda install -c conda-forge python-sounddevice
```
- <b>soundfile</b>
    - This module is used to extract data out of recordings
    - Installation:

```shell
python3 -m pip install soundfile
```
- <b>scipy</b>
    - This module was used to save recorded signals to <i>.wav</i> files and perform importan operations - e.g calculate FFT
    - Installation:
```shell
python3 -m pip install -U scikit-learn
```
---
## Process description
### Recording the signal
<div align="justify">
The script first records user's voice and saves it to a <i>.wav</i> file. The signal is <b>3 seconds</b> long and is sampled with a frequency of <b>44100 Hz</b>. This frequency is broadly used while recording digital signals. The reason why this frequency has a value of 44k Hz is because of hearing abilities. Human hearing frequency ranges from 20 to 20k Hz, so the sampling freqency has to be at least two times bigger in order to properly recreate the signal. This rule stems from <b>Nyquist–Shannon sampling theorem</b>. The picture below presents a plotted recored signal.
</div>

<div align="center">
    <img src="./Signal.png">
</div>

<div align="justify">
Signal is recorded using <i><b>recordVoice</b></i> function. Inside of the function the samplign frequency <b>fs</b> and recording time <b>seconds</b> are specified. After running the function user will receive an information that the recording will soon begin. The recording is saved using <b>rec</b> function from <i>sounddevice</i> library. The recording is then saved to a file.
</div>

<br>

```python
def recordVoice(filename):
    fs = 44100  # Sample rate
    seconds = 3  # Duration of recording
    print("Recording will begin in 3 seconds . . .")
    time.sleep(seconds) # Waiting for the recording to start
    print("Recording. . .")
    myrecording = sd.rec(int(seconds * fs), samplerate=fs, channels=2) # Recording user's voice
    sd.wait()  # Wait until recording is finished
    print("Recording finished !")
    write(filename, fs, myrecording)  # Saving the signal as WAV file 
```
### Estimating signal's parameters
<div align="justify">
The next step is signal analysis. The signal is transformed into frequency domain using Fast Fourier Transform - FFT. By analysing the signal in frequency domain we can determine dominant frequencies in the signal. This will allow us to determine which frequencies are dominant in the signal, which is usefull if we want to determine to what class the signal belongs to - wheather it's A, U or I.
</div>

<div align="center">
    <img src="./FFT.png">
</div>

<div align="justify">
The analysis is performed using <i><b>signalCalc</b></i> function. The recording is transformed into freqency domain using <b>scipy.fftpack</b>. Next step consists of removing frequencies that are irrelevant. In order to do so all frequencies that have an amplitude below a certain threshold will be removed. In our case the threshold's value was chosen using trail and error, until it gave the best results.

The script calculates signal parameters such as mean value of all frequencies, a median frequeny and a standard deviation of frequencies. These parameters will be later used for determinig to which class the recording belongs to.

</div>

```python
def signalCalc(data, threshold):
    idx = [] # Vector containing significant frequency values
    
    fft_tmp = abs(fft(data)) # Transforming the signal using FFT function
    
    for j in range(int(len(fft_tmp)/2)): 
        if fft_tmp[j] > threshold:       # Choosing important frequencies based on a threshold
            idx.append(j)
            
    mean = statistics.mean(idx)         # Mean value of frequencies
    median = statistics.median(idx)     # Median frequency
    std = statistics.stdev(idx)         # Standard deviation of frequencies
```
## Classification

<div align="justify">
To determine wheather the recording is a letter A, U or I the script determines how much it resembles recordings that belong to those categories. 
The pre-recorded signals are saved in <i>.mat</i> files. After importing them they are supplied to <b>matchFunction</b> as signal_batch argument. Then the srcipt calculates parameters such as mean, median and standard deviation for each individual signal in the batch. Afterwards an average of each of those parameters is calculated and then compared with the same parameters of the recorded signal. 
</div>

```python
def matchFunction(signal_batch, signal, threshold):
    meanB, medianB, stdB = parameterCalc(signal_batch, threshold) # Calculationg parameters for a batch of signals
    
    AvgMeanB = statistics.mean(meanB)         # Average 
    AvgMedianB = statistics.mean(medianB)     # Average of median
    AvgStdB = statistics.mean(stdB)           # Average STD
    
    meanSig, medianSig, stdSig = signalCalc(signal, threshold)   # Calculating parameters for the recording
    
    distMean = abs(AvgMeanB - meanSig)                    # Calculating the differences
    distMedian = abs(AvgMedianB - medianSig)
    distStd = abs(AvgStdB - stdSig)
    
    dist = pow((pow(distMean, 2) + pow(distMedian, 2) + pow(distStd, 2)), 0.5)  # Calculating the distance between recording and the average of the batch
    
    return dist

```


## Nearest Neighbour
<div align="center">
    <img src="./NN.png" width="350" height="350">
</div>

Second method consists of finding a nearest neighbour of the recorded signal in the parameter space. It works in a similar way as described above. In the first place we calculate signals parameters. This time we do not estimate an average of for all signals from a batch, but compare our recording to all of them. This way we find a sample signal that is the nearest to our recording. This determines to which category our signal belongs to. Function <b> paramVector</b> is responsible for Nearest Neighbour classification.

```python
def paramVector(signal, signal_batch, threshold):
    # Calculating signal parameters
    meanSig, medianSig, stdSig = signalCalc(signal, threshold) 
    
    # Estimating letter parameters
    meanB, medianB, stdB = parameterCalc(signal_batch, threshold)
    
    # Distance calculation
    
    dist = []
    
    for i in range(len(meanB)):
        D1 = meanSig - meanB[i]        
        D2 = medianSig - medianB[i]
        D3 = stdSig - stdB[i]
        
        dist.append(pow((pow(D1, 2) + pow(D2, 2) + pow(D3, 2)), 0.5))
        
    return dist
```
## Running the script
The program runs on a single script called <b>voiceRecognition.py</b>. Below we can see the script running in </i>help mode</i>. 
Running the script requires specifying <b>mode</b> parameter. Value <b>S</b> represents running the script in standard mode. Value <b>K</b> means the script will use Nearest Neighbour method to determine the class of the recording.

```shell
python3 voiceRecognition.py -h
usage: voiceRecognition.py [-h] mode

This script performs letter recognition procedure by comparing recording parameters to parameters of pre-recorded signals. These signals represent letters A,
U and I.

positional arguments:
  mode        Recognition mode: S - Standard detection mode; K - KNN detection mode

optional arguments:
  -h, --help  show this help message and exit
```

Below we can see an example of how the script is running in Standard mode.
```shell
$ python3 voiceRecognition.py S
Running letter recognition in standard mode
Recording will begin in 3 seconds . . .
Recording. . .
Recording finished !
This is letter I
```
